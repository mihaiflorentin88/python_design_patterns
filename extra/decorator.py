import time

def timing_function(func):
    def wrapper(*args, **kwargs):
        start_ts = time.time()
        func(*args, **kwargs)
        end_ts = time.time()
        print('Execution time: {} seconds.'.format(end_ts - start_ts))
    return wrapper

@timing_function
def awfully_slow_function(slow_factor="0"):
    slow_levels = {
        "0": 0,
        "1": 0.000001,
        "2": 0.00001,
        "3": 0.0001,
        "4": 0.001
    }
    num_list = []
    for i in range(100000):
        num_list.append(i)
        time.sleep(slow_levels.get(slow_factor, "0"))
    print('Sum of all numbers in list: {}'.format(sum(num_list)))


awfully_slow_function("1")
