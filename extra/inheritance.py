class Pet:
    def __init__(self, name, species):
        self.name = name
        self.species = species

    def __str__(self):
        return '{} is a {}.'.format(self.name, self.species)

class Dog(Pet):
    def __init__(self, name, chases_cats):
        super().__init__(name, 'Dog')
        self.chases_cats = chases_cats

    def __str__(self):
        additional_info = ""
        if self.chases_cats:
            additional_info = " He chases cats."
        return super().__str__() + additional_info

dog = Dog(name="Fred", chases_cats=True)
parrot = Pet(name='Polly', species='parrot')

print(dog.__str__())
print(parrot.__str__())
print(Dog.__bases__)
print(Pet.__subclasses__())
