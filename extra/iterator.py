#ITERATOR

# 1. how it works:
my_list = [1, 2, 3]
my_iter = my_list.__iter__()
index_0 = my_iter.__next__()  # fetches the next element from the list (index 0)
print(index_0) # prints 1
index_1 = my_iter.__next__()  # fetches the next element from the list (index 1)
print(index_1) # prints 2

# 2. List comprehension

numbers = [1, 2, 3, 4, 5, 6, 7]
numbers_incremented = [n + 1 for n in numbers]  # for each number (n) in numbers add 1 and save it to the new list.
print(numbers_incremented)  # prints [2, 3, 4, 5, 6, 7, 8]
even_numbers = [n for n in numbers if n % 2 == 0]
print(even_numbers)  # prints [2, 4, 6]
matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
flat = [n for row in matrix for n in row]
print(flat)  # prints [1, 2, 3, 4, 5, 6, 7, 8, 9]
