class Engine:
    def __init__(self):
        self.spin = 0

    def start(self, spin):
        self.spin = min(spin, 3000)
        return self


class StarterMotor:
    def __init__(self):
        self.spin = 0

    def start(self, charge):
        if charge > 25:
            self.spin = 2500


class Battery:
    def __init__(self):
        self.charge = 0


class Car:
    def __init__(self):
        self.battery = Battery()
        self.engine = Engine()
        self.starter = StarterMotor()

    def turn_key(self):
        self.starter.start(self.battery.charge)
        self.engine.start(self.starter.spin)
        if self.engine.spin > 0:
            print('Engine has started!')
        else:
            print('Could not start engine!')

    def jump(self):
        self.battery.charge = 100
        print('Battery charge at {}'.format(self.battery.charge))


car = Car()
car.turn_key()
car.jump()
car.turn_key()
