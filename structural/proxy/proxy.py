class SubjectInterface:
    def request(self):
        raise NotImplementedError


class Proxy(SubjectInterface):
    def __init__(self, real_subject):
        self.real_subject = real_subject


    def request(self):
        print('Watch out! Proxy is doing a request!')
        self.real_subject.request()


class RealSubject(SubjectInterface):
    def request(self):
        print('The real thing is dealing with the request!')


rs = RealSubject()
rs.request()

proxy = Proxy(real_subject=rs)
proxy.request()