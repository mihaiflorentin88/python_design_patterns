class WindowInterface:
    def build(self):
        raise NotImplementedError


class AbstractWindowDecorator(WindowInterface):
    def __init__(self, window: WindowInterface):
        self.window = window

    def build(self):
        raise NotImplementedError


class Window(WindowInterface):
    def build(self):
        print("Built window!")


class BorderDecorator(AbstractWindowDecorator):
    def __init__(self, window: WindowInterface):
        super().__init__(window)
        self.window = window

    def __add_border(self):
        print("Added some border to the window!")

    def build(self):
        self.__add_border()
        self.window.build()


class VerticalScrollbarDecorator(AbstractWindowDecorator):
    def __init__(self, window: WindowInterface):
        super().__init__(window)
        self.window = window

    def __add_scrollbar(self):
        print('Added scrollbar')

    def build(self):
        self.__add_scrollbar()
        self.window.build()

print('Simple window: ')
window = Window()
window.build()
print('\n')

print('Bordered window: ')
bordered_window = BorderDecorator(window)
bordered_window.build()
print('\n')

print('Vertical scrollbar window: ')
vertical_scrollbar_window = VerticalScrollbarDecorator(window)
vertical_scrollbar_window.build()
print('\n')

print('Vertical scrollbar with border window: ')
scrollbar_bordered_window = VerticalScrollbarDecorator(bordered_window)
scrollbar_bordered_window.build()
print('\n')
