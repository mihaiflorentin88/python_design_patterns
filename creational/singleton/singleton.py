class Singleton:
    __instance = None

    def __new__(cls, val=None):
        if Singleton.__instance is None:
            Singleton.__instance = object.__new__(cls)
            Singleton.__instance.val = val
        return Singleton.__instance


a = Singleton()
a.val = 'Potatoes'
print('1rst instance val: {}'.format(a.val))

b = Singleton()
b.val = 'Chips'

print('2nd instance val: {}'.format(b.val))
print('1rst instance val: {}'.format(a.val))
