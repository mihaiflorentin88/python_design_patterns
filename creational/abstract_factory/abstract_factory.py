class Shape2dInterface:
    def draw(self):
        raise NotImplementedError


class Shape3dInterface:
    def build(self):
        raise NotImplementedError


class Circle(Shape2dInterface):
    def draw(self):
        print("CIRCLE DRAW! CIRCLE DRAW! CIRCLE DRAW!")


class Square(Shape2dInterface):
    def draw(self):
        print('SQUARE DRAW!!  SQUARE DRAW!!  SQUARE DRAW!!')


class Sphere(Shape3dInterface):
    def build(self):
        print('SPHERE BUILD!!! SPHERE BUILD!!! SPHERE BUILD!!! ')

class Cube(Shape3dInterface):
    def build(self):
        print('CUBE BUILD!!! CUBE BUILD!!! CUBE BUILD!!! ')

class ShapeFactoryInterface:
    def get_shape(self, shape):
        raise NotImplementedError

class Shape2Dfactory(ShapeFactoryInterface):
    def get_shape(self, shape):
        shapes = {
            'circle': Circle(),
            'square': Square()
        }
        return shapes.get(shape, None)

class Shape3DFactory(ShapeFactoryInterface):
    def get_shape(self, shape):
        shapes = {
            'sphere': Sphere(),
            'cube': Cube()
        }
        return shapes.get(shape, None)


f2d = Shape2Dfactory()
circle = f2d.get_shape('circle')
circle.draw()

f3d = Shape3DFactory()
cube = f3d.get_shape('cube')
cube.build()