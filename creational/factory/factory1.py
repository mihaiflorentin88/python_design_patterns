class ShapeInterface:
    def draw(self):
        raise NotImplementedError


class Circle(ShapeInterface):
    def draw(self):
        print('Yes i am here! I am CIRCLE and you ?')


class Square(ShapeInterface):
    def draw(self):
        print('Yes i am here! I am SQUARE and you ?')


class ShapeFactory():
    @staticmethod
    def get_shape(shape):
        shapes = {
            'circle': Circle(),
            'square': Square()
        }
        return shapes[shape]

square = ShapeFactory.get_shape('square')
square.draw()

circle = ShapeFactory.get_shape('circle')
circle.draw()