class Borg:
    __shared_state = {}

    def __init__(self):
        self.__dict__ = self.__shared_state

a = Borg()
b = Borg()

a.val = 'cornulet cu lapte'
print('Even though the val attribute has only been set for the "a" instance...')
print('"a" instance val: {}'.format(a.val))
print('"b" instance val: {}'.format(b.val))
