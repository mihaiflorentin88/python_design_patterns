from copy import deepcopy

class Car:
    def __init__(self):
        self.__wheels = []
        self.__body = None
        self.__engine = None

    def set_body(self, body):
        self.__body = body

    def set_engine(self, engine):
        self.__engine = engine

    def attach_wheel(self, wheel):
        self.__wheels.append(wheel)

    def specification(self):
        print('Body shape: {}'.format(self.__body.shape))
        print('Engine power: {}'.format(self.__engine.horsepower))
        print('Tire size: {}'.format(self.__wheels[0].size))

    def clone(self):
        """ Prototype pattern """
        return deepcopy(self)


class Wheel:
    def __init__(self, size):
        self.size = size


class Engine:
    def __init__(self, horsepower):
        self.horsepower = horsepower


class Body:
    def __init__(self, shape):
        self.shape = shape


class Director:
    __builder = None

    def set_builder(self, builder):
        self.__builder = builder

    def get_car(self):
        car = Car()
        body = self.__builder.get_body()
        car.set_body(body)

        engine = self.__builder.get_engine()
        car.set_engine(engine)

        for i in range(4):
            wheel = self.__builder.get_wheel()
            car.attach_wheel(wheel)
        return car


class BuilderInterface:
    def get_wheel(self):
        raise NotImplementedError

    def get_engine(self):
        raise NotImplementedError

    def get_body(self):
        raise NotImplementedError


class JeepBuilder(BuilderInterface):
    def get_wheel(self):
        return Wheel(size=22)

    def get_engine(self):
        return Engine(horsepower=400)

    def get_body(self):
        return Body(shape='SUV')


class NissanBuilder(BuilderInterface):
    def get_wheel(self):
        return Wheel(size=16)

    def get_engine(self):
        return Engine(horsepower=100)

    def get_body(self):
        return Body(shape='hatchback')


director = Director()
director.set_builder(JeepBuilder())
jeep = director.get_car()
jeep.specification()
# PROTOTYPE PATTER
jeep2 = jeep.clone()
jeep2.set_engine(Engine(horsepower=229))
jeep2.specification()

