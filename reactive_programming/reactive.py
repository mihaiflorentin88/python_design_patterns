from rx import Observer, Observable

class MyObserver(Observer):
    def on_next(self, value):
        print('Got {}'.format(value))

    def on_error(self, error):
        print('Got {}'.format(error))

    def on_completed(self):
        print('Sequence completed!')

xs = Observable.from_iterable(range(10))
d = xs.subscribe(MyObserver())
print('\n')

xs = Observable.from_(range(10))
d = xs.filter(lambda x: x % 2).subscribe(print)
print('\n')

c = xs.map(lambda x: x * 2).subscribe(print)
print('\n')

xs = Observable.range(1,5)
ys = Observable.from_('abcde')
xs.merge(ys).subscribe(print)
