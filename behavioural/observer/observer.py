class Observable:
    def __init__(self):
        self.observers = []

    def register(self, observer):
        if observer not in self.observers:
            self.observers.append(observer)

    def unregister(self, observer):
        if observer in self.observers:
            self.observers.remove(observer)

    def unregister_all(self):
        del self.observers[:]

    def update_observers(self, *args, **kwargs):
        for observer in self.observers:
            observer.update(*args, **kwargs)


class ObserverInterface:
    def update(self, *args, **kwargs):
        raise NotImplementedError


class AmericanStockMarket(ObserverInterface):
    def update(self, *args, **kwargs):
        print('American stock market recieved: {0}, \n{1}'.format(args, kwargs))


class EuropeanStockMarket(ObserverInterface):
    def update(self, *args, **kwargs):
        print('European stock market recieved: {0}, \n{1}'.format(args, kwargs))


really_big_company = Observable()
american_obs = AmericanStockMarket()
really_big_company.register(american_obs)
european_obs = EuropeanStockMarket()
really_big_company.register(european_obs)

really_big_company.update_observers('important update', msg='CEO just resigned')
