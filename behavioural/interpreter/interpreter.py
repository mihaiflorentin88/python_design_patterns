class ExpressionInterface:
    def interpret(self, text):
        raise NotImplementedError

class TerminalExpression(ExpressionInterface):
    def __init__(self, word):
        self.word = word

    def interpret(self, text) -> bool:
        if self.word in text:
            return True
        return False

class OrExpression(ExpressionInterface):
    def __init__(self, exp1, exp2):
        self.exp1 = exp1
        self.exp2 = exp2

    def interpret(self, text) -> bool:
        return self.exp1.interpret(text) or self.exp2.interpret(text)


class AndExpression(ExpressionInterface):
    def __init__(self, exp1, exp2):
        self.exp1 = exp1
        self.exp2 = exp2

    def interpret(self, text) -> bool:
        return self.exp1.interpret(text) and self.exp2.interpret(text)


john = TerminalExpression('John')
sarah = TerminalExpression('Sarah')
henry = TerminalExpression('Henry')
mary = TerminalExpression('Mary')

rule1 = AndExpression(john, henry)
rule2 = OrExpression(mary, rule1)
rule3 = OrExpression(sarah, rule2)
print(rule3.interpret('Henry'))
print(rule3.interpret('Mary Henry'))
