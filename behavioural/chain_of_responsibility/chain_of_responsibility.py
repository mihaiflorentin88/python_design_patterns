class Car:
    def __init__(self, name, water, fuel, oil):
        self.name = name
        self.water = water
        self.fuel = fuel
        self.oil = oil

    def is_fine(self):
        if self.water >= 20 and self.fuel >= 5 and self.oil >= 10:
            print('HELLO! This is Car. Car is FINE!')
            return True
        return False


class Handler:
    def __init__(self, successor=None):
        self.__successor = successor

    def handle_request(self, car: Car):
        if not car.is_fine() and self.__successor is not None:
            self.__successor.handle_request(car)


class WaterHandler(Handler):
    def handle_request(self, car: Car):
        if car.water <= 20:
            car.water = 100
            print('Added water!')
        super().handle_request(car)


class FuelHandler(Handler):
    def handle_request(self, car: Car):
        if car.fuel <= 5:
            car.fuel = 100
            print('Added fuel!')
        super().handle_request(car)


class OilHandler(Handler):
    def handle_request(self, car: Car):
        if car.oil <= 10:
            car.oil = 100
            print('Added oil!')
        super().handle_request(car)


garage_handler = OilHandler(FuelHandler(WaterHandler()))
car = Car('MyCar', 1, 1, 1)
garage_handler.handle_request(car)
print('\n')

car = Car('MyCar', 10, 10, 10)
garage_handler.handle_request(car)
print('\n')

car = Car('MyCar', 15, 15, 15)
garage_handler.handle_request(car)
print('\n')

car = Car('MyCar', 20, 20, 20)
garage_handler.handle_request(car)
print('\n')
