class ComputerState:
    name = 'state'
    allowed = []

    def switch(self, state):
        if state.name in self.allowed:
            print('Current state {} => switched to {}.'.format(self, state.name))
            self.__class__ = state.__class__
        else:
            print('Current state {} => switching to {} not possible.'.format(self, state.name))

    def __str__(self):
        return self.name


class On(ComputerState):
    name = 'on'
    allowed = ['off', 'suspend', 'hibernate']


class Off(ComputerState):
    name = 'off'
    allowed = ['on']


class Suspend(ComputerState):
    name = 'suspend'
    allowed = ['on']


class Hibernate(ComputerState):
    name = 'hibernate'
    allowed = ['on']


class Computer:
    def __init__(self):
        self.state = Off()

    def change(self, state: ComputerState):
        self.state.switch(state)


computer = Computer()
print(computer.state.__str__())

on = On()
off = Off()
suspend = Suspend()
hibernate = Hibernate()

computer.change(on)
print(computer.state.__str__())

computer.change(off)
print(computer.state.__str__())

computer.change(on)
computer.change(suspend)
print(computer.state.__str__())

computer.change(on)
computer.change(hibernate)
print(computer.state.__str__())

