class MakeMeal:
    def buy_ingredients(self, money):
        if money < self.cost:
            assert 0, 'Not enough money to buy ingredients'

    def prepare(self):
        raise NotImplementedError

    def cook(self):
        raise NotImplementedError

    def go(self, money):
        self.buy_ingredients(money)
        self.prepare()
        self.cook()


class MakePizza(MakeMeal):
    def __init__(self):
        self.cost = 3

    def prepare(self):
        print('Prepare pizza!')

    def cook(self):
        print('Cook pizza!')


class MakeCake(MakeMeal):
    def __init__(self):
        self.cost = 2

    def prepare(self):
        print('Prepare cake!')

    def cook(self):
        print('Cook cake!')

pizza = MakePizza()
pizza.go(5)

cake = MakeCake()
cake.go(1)





